#include "Animation.h"

void main()
{
	RenderWindow window(VideoMode(800, 600), "AnimationManager");

	Texture tRun;
	tRun.loadFromFile("animations\\run.png");
	Texture tDeath;
	tDeath.loadFromFile("animations\\death.png");
	Texture tStay;
	tStay.loadFromFile("animations\\stay.png");
	Texture tShoot;
	tShoot.loadFromFile("animations\\shoot.png");
	AnimationManager am;

	am.AutoSplitFrames("run", tRun, true, 0.01, 3, 5);
	am.AutoSplitFrames("death", tDeath, false, 0.01, 3, 5);
	am.AutoSplitFrames("shoot", tShoot, false, 0.03, 2, 8);
	am.AutoSplitFrames("stay", tStay, true, 0.01, 2, 6);
	am.SetAnim("run");
	
	am.Play();

	bool flip=false;
	
	Clock clock;
	double time;
	while (window.isOpen())
	{
		time = clock.getElapsedTime().asMicroseconds();
		time /= 800;
		clock.restart();

		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
		}
		if (Keyboard::isKeyPressed(Keyboard::Num1))
		{
			am.SetAnim("death");
			am.Play();
		}
		if (Keyboard::isKeyPressed(Keyboard::Num2))
		{
			am.SetAnim("shoot");
			am.Play();
		}
		if (Keyboard::isKeyPressed(Keyboard::Num3))
		{
			am.SetAnim("stay");
			am.Play();
		}
		if (Keyboard::isKeyPressed(Keyboard::Right))
		{
			flip=false;
		}
		if (Keyboard::isKeyPressed(Keyboard::Left))
		{
			flip = true;
		}
		if (Keyboard::isKeyPressed(Keyboard::Num4))
		{
			am.SetAnim("run");
			am.Play();
		}

		am.SetFlip(flip);

		am.Tick(time);
		window.clear(Color::White);
		am.Draw(window, Vector2f(10, 10));
		window.display();
	}
}
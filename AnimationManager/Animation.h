#pragma once

#include "SFML\Graphics.hpp"
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include "TinyXML\tinyxml.h"

using namespace std;
using namespace sf;

class AnimationManager;

class Animation
{
	vector<IntRect> frames, //�����
	framesFlip;//�������������
	double curFrame,//������� ����
		speed;//�������� ������������
	bool isPlaying,//�������� ��
		loop,//��������� ��
		flip;//���������� ��
	Sprite sprite;
public:
	friend class AnimationManager;

	Animation();
	Animation(Texture&texture,IntRect&rect,double sp,int count,int step,bool loop);
	~Animation();

	void Tick(double time);
	void SetFlip(bool fl);
	Vector2i GetSize();
};

class AnimationManager 
{
	string curAnim;//��� ������� ��������
	map<string, Animation> animList;
public:
	AnimationManager();
	~AnimationManager();

	void Create(string name, Texture&texture, IntRect&rect, double speed, int count, int step, bool loop);//������� �������
	void Draw(RenderWindow&window,Vector2f&v);//���������� �������
	void SetAnim(string name);//������ �������
	void Tick(double time);//����������� ������� ��������
	void Play();//������ �������
	void Pause();//���������� �������
	void SetFlip(bool flip);
	Vector2i GetSizeCurAnim();//�������� ������ ������� �������� 
	void loadFromXML(std::string fileName, Texture &t);
	void AutoSplitFrames(string name, Texture &texture, bool loop, double speed, int row, int col, int w = 0, int h = 0);

};


#include "Animation.h"


Animation::Animation()
{
	curFrame = 0;
	flip = false;
	isPlaying = false;
	loop = true;
	speed = 0;
}
Animation::Animation(Texture&texture, IntRect&rect, double sp, int count, int step, bool loop)
{
	sprite.setTexture(texture);
	speed = sp;
	this->loop = loop;
	curFrame = 0;
	flip = false;
	for (size_t i = 0; i < count; i++)
	{
		frames.push_back(IntRect(rect.left + i*step, rect.top, rect.width, rect.height));
		framesFlip.push_back(IntRect(rect.left + i*step + rect.width, rect.top, -rect.width, rect.height));
	}
}
Animation::~Animation()
{
}
void Animation::Tick(double time)
{
	if (!isPlaying)//���� �� ��������
	{
		return;
	}
	curFrame += speed*time;
	if (curFrame>frames.size())//���� ������ ��� ����� ��������
	{
		
		if (!loop)//���� �� ���������
		{
			isPlaying = false;//���������� ��������
			curFrame -= 1;
		}
		else
		{
			curFrame= 0;
		}
	}
	int i = curFrame;
	if (flip)//���� �����������
	{
		sprite.setTextureRect(framesFlip[i]);
	}
	else
	{
		sprite.setTextureRect(frames[i]);
	}
}
void Animation::SetFlip(bool fl)
{
	flip = fl;
}
Vector2i  Animation::GetSize()
{
	int i = curFrame;
	return Vector2i(frames[i].width, frames[i].height);
}

AnimationManager::AnimationManager()
{
}
AnimationManager::~AnimationManager()
{
}
void AnimationManager::Create(string name, Texture&texture, IntRect&rect, double speed, int count, int step, bool loop)//������� �������
{
	animList[name] = Animation(texture, rect, speed, count, step, loop);
	curAnim = name;
}
void AnimationManager::Draw(RenderWindow&window, Vector2f&v)//���������� �������
{
	animList[curAnim].sprite.setPosition(v);
	window.draw(animList[curAnim].sprite);
}
void AnimationManager::SetAnim(string name)//������ �������
{
	animList[curAnim].curFrame = 0;
	curAnim = name;
}
void AnimationManager::Tick(double time)//����������� ������� ��������
{
	animList[curAnim].Tick(time);
}
void AnimationManager::Play()//������ �������
{
	animList[curAnim].isPlaying=true;
}
void AnimationManager::Pause()//���������� �������
{
	animList[curAnim].isPlaying = false;
}
void AnimationManager::SetFlip(bool flip)
{
	animList[curAnim].flip = flip;
}
Vector2i AnimationManager::GetSizeCurAnim()//�������� ������ ������� �������� 
{
	return animList[curAnim].GetSize();
}
void AnimationManager::loadFromXML(std::string fileName, Texture &t)
{
	cout << 3 << endl;
	TiXmlDocument animFile(fileName.c_str());

	animFile.LoadFile();

	TiXmlElement *head;
	head = animFile.FirstChildElement("sprites");

	TiXmlElement *animElement;
	animElement = head->FirstChildElement("animation");
	while (animElement)
	{
		Animation anim;

		curAnim = animElement->Attribute("title");
		int delay = atoi(animElement->Attribute("delay"));
		anim.speed = 0.01;
		anim.sprite.setTexture(t);

		TiXmlElement *cut;
		cut = animElement->FirstChildElement("cut");
		while (cut)
		{
			int x = atoi(cut->Attribute("x"));
			int y = atoi(cut->Attribute("y"));
			int w = atoi(cut->Attribute("w"));
			int h = atoi(cut->Attribute("h"));

			anim.frames.push_back(IntRect(x, y, w, h));
			anim.framesFlip.push_back(IntRect(x + w, y, -w, h));
			cut = cut->NextSiblingElement("cut");
			cout << 2 << endl;
		}

		//anim.sprite.setOrigin(0, anim.frames[0].height);

		animList[curAnim] = anim;
		animElement = animElement->NextSiblingElement("animation");
		cout << 1 << endl;
	}

}
void AnimationManager::AutoSplitFrames(string name, Texture &texture, bool loop, double speed, int row, int col, int w, int h)
{
	Animation anim;
	anim.speed = speed;
	anim.loop = loop;
	anim.sprite.setTexture(texture);
	w = texture.getSize().x / (double)col;
	h = texture.getSize().y / (double)row;
	for (size_t i = 0; i < row; i++)
	{
		for (size_t j = 0; j < col; j++)
		{
			anim.frames.push_back(IntRect(j*w, i*h, w, h));
			anim.framesFlip.push_back(IntRect(j*w + w, i*h, -w, h));
		}
	}
	animList[name] = anim;
	curAnim = name;
}